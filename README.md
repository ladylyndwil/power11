# Power11
### The all in one user friendly OOBE for Windows 11

Did you know that Microsoft hides many options from most users on their OS's? Anyone in it will be familiar with the vast reconfigurability of Windows, but the typical Windows user will never touch, or even know about most of the capability of the OS.

**Power11** aims to provide an improved Windows OOBE which provides normal users far more control over their OS installation and still complies with Microsoft's license. Users will be able to exercise far more granular control over their installation including Windows features, and the installation and configuration of 3rd party software.

*This project is in development and does not have a working build.*
